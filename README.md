# CE_III_48_Lab1

1. Implement linear and binary search algorithms.
2. Write some test cases to test your program.
3. Generate some random inputs for your program and apply both linear
and binary search algorithms to find a particular element on the generated
input. Record the execution times of both algorithms for best and worst
cases on inputs of different size (e.g. from 10000 to 100000 with step size
as 10000). Plot an input-size vs execution-time graph.
4. Explain your observations.


1. The search algorithms are implemented in the searchAlgo.py File
2. unittest was used to use test case for both the search algorithms in UnitTest.py file
3. The graph was plotted and instructions as instructed were performed in the Plot.py file
4. The graph for the linear time varies linearly with time but as the binary search should include the sorting part 
    for the searching. Most of the time taken for the binary search was due to the sorting of the data
    rather than the actual sort.